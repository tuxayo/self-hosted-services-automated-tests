# coding: utf-8
When(/^I go to the Roundcube page$/) do
  visit '/mail'
end

When(/^I enter my email credentials$/) do
  fill_in "rcmloginuser", with: ENV["EMAIL_USER"]
  fill_in "rcmloginpwd", with: ENV["EMAIL_PASSPHRASE"]
  click_on "Connexion"
end

Then(/^I see my inbox$/) do
  check_that_inbox_loaded()
end


def check_that_inbox_loaded(retries_count=0)
  begin
    expect(page).to have_content "this is an email in the mailbox"
  rescue RSpec::Expectations::ExpectationNotMetError => error
    # in more than 1300 runs, it was never needed more than 1 retry
    # with a wait time of 15, that means emails load in less than 30 sec
    if retries_count >= 5 # first try + 5 retries = 30 seconds max
      raise error
    end
    log "Inbox failed to load, reloading page"
    visit current_path
    check_that_inbox_loaded(retries_count+1)
  end
end

When(/^I click on the compose button$/) do
  find(".action-buttons .compose").click
end

When(/^I compose an email to myself$/) do
  find("#compose_to input").set ENV["EMAIL_USER"]
  fill_in "compose-subject", with: "This is an email to myself"
  fill_in "composebody", with: "This is the body"
end

When(/^send it$/) do
  # forcing the click to bypass an eventual message about an unsent draft
  find(".formbuttons .send").click
end

Then(/^I should see my email in the mailbox$/) do
  check_email_arrived_in_inbox()
end

def check_email_arrived_in_inbox(retries_count=0)
  Capybara.default_max_wait_time = 10
  begin
    expect(page).to have_selector(".message.unread .subject")
    expect(page).to have_content "This is an email to myself"
  rescue RSpec::Expectations::ExpectationNotMetError => error
    # worst time to arrive observed yet:
    # with wait time of 10, 6 tries in total → comes in less than 60 sec
    if retries_count >= 7 # first try + 7 retries = 80 seconds max
      raise error
    end
    log "Email not here yet, reloading page"
    visit current_path
    check_email_arrived_in_inbox(retries_count+1)
  end
  # not in ensure block because it doesn't matter if not reset in case of failure
  Capybara.default_max_wait_time = DEFAULT_MAX_WAIT_TIME
end

When(/^I delete it$/) do
  test_message = find_unread_test_messages()[0]
  delete_message(test_message)
end

Then(/^it should not be there$/) do
  expect(page).to_not have_content "This is an email to myself"
end

Then(/^I check if there are not emails from previous tests$/) do
  test_messages = find_test_messages()
  if test_messages.size > 0
    log "Found #{test_messages.size} old test message to delete"
    delete_messages(test_messages)
  end
end

def find_unread_test_messages
  unread_messages = find_all(".message.unread span.subject")
  unread_test_messages = unread_messages.select{
    | message | message.has_content?("This is an email to myself")
  }
end

def find_test_messages
  messages = find_all(".message span.subject")
  test_messages = messages.select{
    | message | message.has_content?("This is an email to myself")
  }
end

def delete_messages(messages_capybara_object)
  messages_capybara_object.each {
    | message | delete_message(message)
  }
end

def delete_message(message_capybara_object)
  message_capybara_object.click

  # we check & wait that the message has loaded
  within_frame "messagecontframe" do
    expect(page).to have_selector "#messagebody"
  end

  find("#toolbar-menu .delete").click
  begin
    expect(page).to have_selector "#messagestack .confirmation.alert-success"
  rescue RSpec::Expectations::ExpectationNotMetError
    log "We have to retry to delete the email"
    find("#toolbar-menu .delete").click
    expect(page).to have_selector "#messagestack .confirmation.alert-success"
  end
end
