require 'whois-parser'

When(/^I get the WHOIS data of the domain$/) do
  whois_client = Whois::Client.new(timeout: 20)
  whois_record = get_whois_record(whois_client)
  @whois_parser = whois_record.parser
end

def get_whois_record(whois_client)
  begin
    whois_client.lookup(ENV['HOST'])
  rescue Timeout::Error
    # retry
    log "Timeout, retrying"
    whois_client.lookup(ENV['HOST'])
  end
end

Then(/^it should expire in more than (\d+) days$/) do |days|
  remaining_valid_days = @whois_parser.expires_on.to_date - DateTime.now.to_date
  log "Remaining valid days: #{remaining_valid_days.to_i}"
  expect(remaining_valid_days).to be >= days.to_i
end
