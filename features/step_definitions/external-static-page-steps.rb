When("I go to example.org") do
  visit 'https://example.org'
end

Then("I see the example text") do
  expect(page).to have_content "This domain is for use in illustrative examples in documents."
end
