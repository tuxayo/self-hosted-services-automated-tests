When(/^I go to the home page$/) do
  visit '/'
end

Then(/^I see the contact email$/) do
  expect(page).to have_content 'victor (replace by @) tuxayo DOT net'
end
