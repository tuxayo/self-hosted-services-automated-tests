When(/^I go to the ownCloud page$/) do
  visit ENV["OWNCLOUD_URL"]
end

Then(/^I see the ownCloud login page$/) do
  expect(page).to have_content "Nextcloud"
  expect(page).to have_css "#user"
end

When(/^I enter my ownCloud credentials$/) do
  fill_in "user", with: ENV["OWNCLOUD_USER"]
  fill_in "password", with: ENV["OWNCLOUD_PASSPHRASE"]
  click_on "submit-form"
end

Then(/^I see my files$/) do
  Capybara.default_max_wait_time = 20
  expect(page).to have_content "welcome.txt"
  Capybara.default_max_wait_time = DEFAULT_MAX_WAIT_TIME
end

When(/^I open the test file$/) do
  find('[href$="welcome.txt"]').click
end

Then(/^I see the test file content$/) do
  expect(page).to have_selector "#editor-container"
  expect(page).to have_selector ".editor__content"
  expect(page).to have_content "This is just an example file"
end
