require 'socket'
require 'openssl'

When(/^I connect on port (\d+)$/) do |port|
  tcp_socket = TCPSocket.new(ENV['HOST'], 443)
  ssl_context = OpenSSL::SSL::SSLContext.new()
  ssl_context.set_params(verify_mode: OpenSSL::SSL::VERIFY_PEER)
  @ssl_socket = OpenSSL::SSL::SSLSocket.new(tcp_socket, ssl_context)
  @ssl_socket.connect
  tcp_socket.close
  @ssl_socket.sysclose
end

Then(/^there should be an X.509 certificate$/) do
  @cert = OpenSSL::X509::Certificate.new(@ssl_socket.peer_cert)
end

Then(/^it should be valid for at least (\d+) days$/) do |days|
  remaining_valid_days = @cert.not_after.to_date - DateTime.now.to_date
  log "Remaining valid days: #{remaining_valid_days.to_i}"
  expect(remaining_valid_days).to be >= days.to_i
end
