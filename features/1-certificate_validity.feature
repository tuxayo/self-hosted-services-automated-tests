Feature: Certificate validity
  Scenario: Check certificate
    When I connect on port 443
    Then there should be an X.509 certificate
    And it should be valid for at least 28 days
