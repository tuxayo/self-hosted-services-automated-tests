Feature: Domain expiration
  Scenario: Domain expiration
    When I get the WHOIS data of the domain
    Then it should expire in more than 20 days
