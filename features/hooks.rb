After do |scenario|

  if scenario.failed?
    file_dump_path = "tmp/cucumber_last_fail_for_#{scenario.name}".tr(" ", "_")
    screenshot_path = file_dump_path + ".png"
    page_dump_path = file_dump_path + ".html"

    log "current_path: \"#{current_path}\""
    log "for details see these files:"
    log screenshot_path
    log page_dump_path

    page.save_screenshot(screenshot_path)

    File.open(page_dump_path, 'w') do |file|
      file.puts page.html
    end

  end

end
