Feature: Emails with Roundcube
  Scenario: Roundcube
    When I go to the Roundcube page
    And I enter my email credentials
    Then I see my inbox
    And I check if there are not emails from previous tests
    When I click on the compose button
    And I compose an email to myself
    And send it
    Then I see my inbox
    Then I should see my email in the mailbox
    When I delete it
    Then it should not be there
