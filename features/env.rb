require 'dotenv'
require 'capybara'
require 'capybara/rspec'
require 'capybara/cucumber'
require 'selenium-webdriver'

include RSpec::Matchers

Dotenv.load

if ENV['HOST'] == nil
  log 'You must copy ".env.default" to ".env" and customize it with your settings'
  log 'Aborting'
  exit false
end

Capybara.register_driver :selenium do |app|
  options = Selenium::WebDriver::Firefox::Options.new(args: ['-headless'])
  Capybara::Selenium::Driver.new(app, :browser => :firefox, :options => options)
end

DEFAULT_MAX_WAIT_TIME = 5

Capybara.configure do |config|
  config.run_server = false
  config.app_host = 'https://' + ENV['HOST']
  config.default_driver = :selenium
  config.default_max_wait_time = DEFAULT_MAX_WAIT_TIME
end
