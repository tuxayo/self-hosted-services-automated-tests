Feature: ownCloud
  Scenario: ownCloud
    When I go to the ownCloud page
    Then I see the ownCloud login page
    When I enter my ownCloud credentials
    Then I see my files
    When I open the test file
    Then I see the test file content
